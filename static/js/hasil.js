// Pemanggilan JSON

$(document).ready(function() {
    $('#ini-hasil').empty()
    $.ajax({
        url : "/profile/cobalist/",
        dataType: "json",
        type:"get",
        success: function(data,status) {
            for (i=(data['content'].length-1); i>=0; i--) {
                var nama = data['content'][i]['nama'];
                var umur = data['content'][i]['umur'];
                var domisili = data['content'][i]['domisili'];
                var berat_badan = data['content'][i]['berat'];
                var tinggi_badan = data['content'][i]['tinggi'];                   

                var html = `  
                <div class="col-md-8">
                    <h1>Profile Info</h1>
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Name</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${nama}
                                </div>
                            </div>
                        <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                    <h6 class="mb-0">Age </h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${umur}
                                </div>
                            </div>
                        <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                <h6 class="mb-0">Residence</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${domisili}
                                </div>
                            </div>
                        <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                <h6 class="mb-0">Weight</h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${berat_badan} Kg
                                </div>
                            </div>
                        <hr>
                            <div class="row">
                                <div class="col-sm-3">
                                <h6 class="mb-0">Height </h6>
                                </div>
                                <div class="col-sm-9 text-secondary">
                                    ${tinggi_badan} Cm
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
`
            $('#ini-hasil').append(html);
            }
        }
    })
    
})
