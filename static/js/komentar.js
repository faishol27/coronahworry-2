var POSTID;
var APIURL = {
    "postKomentar": "/curhat/sendkomentar/",
    "getKomentar": "/curhat/getkomentar/",
    "getLastKomentar": "/curhat/lastkomentar/",
    "postReaction": "/curhat/sendreaction/",
    "getReaction": "/curhat/getreaction/"
}

var komentarLength = 0;
var komentarInputNumrow = 1;
function updateKomentarInput() {
    var komentar = $('#komentar-input').val()
    komentarLength = komentar.length
    komentarInputNumrow = komentar.split("\n").length
    $("#komentar-length").text(komentarLength)
    
    if (komentarInputNumrow > 5){
        $('#komentar-input').attr("rows", komentarInputNumrow)
    } else {
        $('#komentar-input').attr("rows", 5)
    }
}
function sendKomentar() {
    var content = $('#komentar-input').val().trim()
    if (content.length > 0) {
        let data = {
            "postinganId": POSTID,
            "content": content
        }

        $.post(APIURL['postKomentar'], data, () => {
            $('#komentar-input').val("")
            updateKomentarInput()
            updateKomentar()
        }).fail(() => {
            alert("Gagal mengirimkan komentar.")
        });

    }
}

$('#komentar-input').keyup((event) => {
    if (event.keyCode == 13 && event.shiftKey == false) {
        sendKomentar()
    }

    updateKomentarInput()
});
$('#komentar-input').keypress(updateKomentarInput);

$('#komentar-input').focus(() => {
    if (komentarInputNumrow < 5) {
        $('#komentar-input').attr("rows", "5")
    } else {
        $('#komentar-input').attr("rows", komentarInputNumrow)
    }
});
$('#komentar-input').focusout(() => {
    $('#komentar-input').attr("rows", komentarInputNumrow)
});

$('#btn-reaction').mouseenter(() => {
    $("#reaction-bar").show()
});

var mouseTracker = [];
$(document).mouseover((event) => {
    mouseTracker.push(event.target)
    if (mouseTracker.length > 100 && mouseTracker.indexOf($('#reaction-bar')[0]) == -1) {
        mouseTracker = [];
    }
});
$('#btn-reaction').mouseleave(() => {
    $("#reaction-bar").mouseleave(() => {
        $("#reaction-bar").hide("10")
        mouseTracker = [];
    })
    setTimeout(() => {
        if (mouseTracker.indexOf($('#reaction-bar')[0]) == -1) {
            $("#reaction-bar").hide("10")
        }
        mouseTracker = [];
    }, 500);
});
var EMOTICON = ['expressionless', 'hearts', 'confused', 'angry']
var lastReaction = -1;
function updateReaction() {
    let query = `?postinganId=${POSTID}`;
    $.get(APIURL['getReaction'] + query, (data) => {
        lastReaction = parseInt(data["reaction"])
        let imgReaction = EMOTICON[lastReaction];
        $("#btn-reaction > img").attr("src", `/static/img/komentar/${imgReaction}.svg`)
    }).fail(() => {
        alert("Gagal mengupdate reaction.")
    });
}
$('.reaction-item').click((event) => {
    let reactionId = $(event.currentTarget).attr("data");
    if (lastReaction == parseInt(reactionId)) reactionId = 0;
    let data = {
        "postinganId": POSTID,
        "reaction": reactionId
    }
    $.post(APIURL['postReaction'], data, updateReaction).fail(() => {
        alert("Gagal mengirimkan reaction.")
    });
})

var komentarLastPk = -1;
var komentarUpdBtn = false;
function addKomentarUpdButton() {
    if (komentarUpdBtn == true) return;
    let html = `<div id="komentar-upd" class="text-center">
        <button class="btn btn-primary" style="border-radius: 30px;" onclick="javascript:updateKomentar();">Lihat komentar terbaru</button>
    </div>`;
    $("#komentar-form").prepend(html)
    komentarUpdBtn = true;
}
function removeKomentarUpdButton() {
    if (komentarUpdBtn == false) return;
    $("#komentar-upd").remove()
    komentarUpdBtn = false;
}
function parseKomentarItem(data, id = 0) {
    let nama = data['user'];
    let komentar = data['content'].replaceAll("\n", "<br />");
    let html = `<div class="komentar-item">
        <div class="font-weight-bold">${nama}</div>
        <div>${komentar}</div>
        <hr>
    </div>`;

    if (id == 0) {
        $('#komentar-bar').prepend(html)
    } else {
        $('#komentar-bar').append(html)
    }
}
function updateKomentar() {
    let query = `?postinganId=${POSTID}&&start=${komentarLastPk + 1}`;
    $.getJSON(APIURL['getKomentar'] + query, (data) => {
        let lenData = data['content'].length;
        for (let i = 0; i < lenData; i++) {
            parseKomentarItem(data['content'][i], 1);
        }
        komentarLastPk = data['content'][lenData - 1]['pk'];
        removeKomentarUpdButton();
    }).fail(() => {
        alert("Gagal mengupdate komentar.");
    });
}
function isKomentarNeedUpdate() {
    let query = `?postinganId=${POSTID}`;
    $.getJSON(APIURL['getLastKomentar'] + query, (data) => {
        if (data['komentarLast'] != komentarLastPk) {
            addKomentarUpdButton()
        }
    }).fail(() => {
        alert("Gagal mengecek komentar terbaru.");
    });
}

$(document).ready(() => {
    POSTID = $("#postingan-id").val()
    updateReaction()
    updateKomentar()
    komentarLength = $('#komentar-input').val().length
    setInterval(isKomentarNeedUpdate, 5000)
});