//melakukan pemanggilan dan pengambilan data dengan ajax
$(document).ready(function() {
    console.log("masukkkkk");
    $('#hasil').empty()
    $.ajax({
        url : "/workout/cobalist/",
        dataType: "json",
        type:"get",
        success: function(data,status) {
            console.log("jajajaj");
            var panjang = data['content'].length;
            console.log(panjang);
            for (i=(data['content'].length-1); i>=0; i--) {
                var nama = data['content'][i]['user'];
                var judul = data['content'][i]['title'];
                var desc = data['content'][i]['desc'];
                var published = data['content'][i]['published'];
                var duration = data['content'][i]['duration'];                
            
                var html = `<div class="col-md-6 col-sm-12 mb-3 id="ini-apa">
                        <div class="card box-shadow py-2 px-4" id="card">
                            <div class="text-xs font-weight-bold mb-1">  
                              ${nama}
                            </div>
                            <div class="card-body" style="display:grid; grid-template-columns: auto 1fr; row-gap: .3rem; column-gap: .8rem;">
                                <div>Judul</div><div>${judul}</div>
                                <div>Published</div><div>${published}</div>
                                <div>Duration</div><div>${duration}</div>
                                <div>Deskripsi</div><div style="white-space: nowrap; overflow: hidden;"><div style="white-space: initial;">${desc}</div></div>
                            </div>
                        </div>
                    </div>
                    `
                $('#ini-hasil').append(html);
            }
        }
    })
})
