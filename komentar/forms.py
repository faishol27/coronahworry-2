from django import forms
from komentar.models import Komentar

class KomentarForm(forms.ModelForm):
    class Meta:
        model = Komentar
        fields = ("content", )
    
    def save(self, user, postingan = None, commit=True):
        komentar = super(KomentarForm, self).save(commit=False)
        komentar.postingan = postingan
        komentar.user = user
        if commit:
            komentar.save()
        return komentar