from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from komentar.models import Like, Komentar
from komentar.forms import KomentarForm
from postingan.models import Post as Postingan

def index(req, postid):
    if req.user.is_authenticated:
        try:
            context = {}

            komentarForm = KomentarForm()
            post = Postingan.objects.get(pk = postid)    
            
            if req.method == 'POST':
                komentarForm = KomentarForm(req.POST)
                if komentarForm.is_valid():
                    komentarForm.save(req.user, post)

            likes = Like.objects.filter(user = req.user).filter(postingan = post)
            
            context['postingan'] = post
            context['comments'] = Komentar.objects.filter(postingan = post)
            context['is_like'] = likes.count()
            context['form'] = komentarForm

            return render(req, 'komentar/komentar.html', context)
        except Exception as ex:
            return redirect('postingan:index')
    else:
        return redirect('main:login')

def post_like(req, postid):
    if req.user.is_authenticated:
        try:
            post = Postingan.objects.get(pk = postid)
            likes = Like.objects.filter(user = req.user).filter(postingan = post)
            if likes.count() > 0:
                likes.delete()
            else:
                Like.objects.create(user = req.user, postingan = post)
            return redirect('komentar:index', postid)
        except:
            return redirect('postingan:index')
    else:
        return redirect('main:login')

def get_komentar(req):
    respJson = {"error": "missing postinganId"}
    status_code = 400
    try:
        if req.GET.get('postinganId') != None:
            postinganId = req.GET.get('postinganId')
            start = int(req.GET.get('start', -1))
            end = int(req.GET.get('end', -1))

            post = Postingan.objects.get(pk = postinganId)
            komentars = Komentar.objects.filter(postingan = post)

            respContent = []
            for komentar in komentars:
                if start != -1 and komentar.pk < start:
                    continue
                if end != -1 and komentar.pk > end:
                    continue
                tmp = {
                    "pk": komentar.pk,
                    "user": komentar.user.username,
                    "content": komentar.content
                }
                respContent.append(tmp)
            respJson = {"count": len(respContent), "content": respContent}
            status_code = 200
    except ObjectDoesNotExist:
        respJson["error"] =  "cannot find postingan with id = postinganId"
    except ValueError:
        respJson["error"] = "something went wrong"
        status_code = 500

    return JsonResponse(respJson, status = status_code)

def last_komentar(req):
    respJson = {"error": "missing postinganId"}
    status_code = 400
    try:
        if req.GET.get('postinganId') != None:
            postinganId = req.GET.get('postinganId')
            
            post = Postingan.objects.get(pk = postinganId)
            komentars = Komentar.objects.filter(postingan = post).order_by('-pk')

            lastPK = -1
            if komentars.count() > 0:
                lastPK = komentars[0].pk

            respJson = {"komentarLast": lastPK}
            status_code = 200
    except ObjectDoesNotExist:
        respJson["error"] =  "cannot find postingan with id = postinganId"
    except ValueError:
        respJson["error"] = "something went wrong"
        status_code = 500

    return JsonResponse(respJson, status = status_code)

@csrf_exempt
def send_komentar(req):
    respJson = {"error": "method not allowed"}
    status_code = 405

    if req.method == 'POST':
        try:
            if not req.user.is_authenticated:
                raise Exception({"message": "forbidden", "status_code":403})
            
            postinganId = req.POST.get('postinganId')
            content = req.POST.get('content')

            if postinganId == None or content == None or len(content) > 200:
                raise Exception({"message": "invalid form", "status_code":400})
                
            post = Postingan.objects.get(pk = postinganId)
            komentar = Komentar.objects.create(user = req.user, postingan = post, content = content)

            respJson = {"status":"ok"}
            status_code = 200
        except ObjectDoesNotExist:
            respJson["error"] =  "cannot find postingan with id = postinganId"
            status_code = 400
        except ValueError:
            respJson["error"] = "something went wrong"
            status_code = 500
        except Exception as ex:
            respJson["error"] =  ex.args[0]['message']
            status_code = ex.args[0]['status_code']


    return JsonResponse(respJson, status = status_code)
    
def get_reaction(req):
    status_code = 500
    respJson = {'error': 'something went wrong'}
    try:
        if not req.user.is_authenticated:
            raise Exception({"message": "forbidden", "status_code":403})
                
        postinganId = req.GET.get('postinganId')
        if postinganId == None:
            raise Exception({"message": "missing postinganId", "status_code":400})
        
        post = Postingan.objects.get(pk = postinganId)
        reaction = Like.objects.filter(user = req.user).filter(postingan = post)
        
        if reaction.count() == 0:
            respJson = {'reaction' : 0}
        else:
            respJson = {'reaction' : reaction[0].reaction}
        status_code = 200
    except ObjectDoesNotExist:
        respJson["error"] =  "cannot find postingan with id = postinganId"
        status_code = 400
    except ValueError:
        respJson["error"] = "something went wrong"
        status_code = 500
    except Exception as ex:
        respJson["error"] =  ex.args[0]['message']
        status_code = ex.args[0]['status_code']

    return JsonResponse(respJson, status = status_code)

@csrf_exempt
def send_reaction(req):
    respJson = {"error": "method not allowed"}
    status_code = 405

    if req.method == 'POST':
        try:
            if not req.user.is_authenticated:
                raise Exception({"message": "forbidden", "status_code":403})
            
            postinganId = req.POST.get('postinganId')
            reactionId = req.POST.get('reaction')

            if postinganId == None or reactionId == None or not postinganId.isdigit() or not reactionId.isdigit():
                raise Exception({"message": "invalid form", "status_code":400})
            if int(reactionId) < 0 or int(reactionId) > 3:
                raise Exception({"message": "invalid form", "status_code":400})
                 
            post = Postingan.objects.get(pk = postinganId)
            reactionOld = Like.objects.filter(user = req.user).filter(postingan = post)
            
            if reactionOld.count() == 0:
                Like.objects.create(user = req.user, postingan=post, reaction=reactionId)
            else:
                reaction = reactionOld[0]
                reaction.reaction = reactionId
                reaction.save()

            respJson = {"status":"ok"}
            status_code = 200
        except ObjectDoesNotExist:
            respJson["error"] =  "cannot find postingan with id = postinganId"
            status_code = 400
        except Exception as ex:
            respJson["error"] =  ex.args[0]['message']
            status_code = ex.args[0]['status_code']

    return JsonResponse(respJson, status = status_code)