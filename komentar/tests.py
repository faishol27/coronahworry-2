from django.core import serializers
from django.contrib.auth.models import User
from django.test import TestCase, Client

from komentar.models import Komentar, Like
from postingan.models import Post as Postingan

import json

class KomentarLoginTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        user = User.objects.get(pk = 1)
        Postingan.objects.create(user = user, message = 'Halooo')
        self.client.login(username = 'test', password = 'HaloHalo12345678')
        
    def test_postingan_not_existed_like(self):
        REDIRECT_EXP = [('/curhat/', 302)]
        resp = self.client.get('/curhat/1000000/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 0)
    
    def test_postingan_existed_add_like(self):
        REDIRECT_EXP = [('/curhat/1/view/', 302)]
        resp = self.client.get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 1)
    
    def test_postingan_existed_remove_like(self):
        user = User.objects.get(username = 'test')
        post = Postingan.objects.get(pk = 1)
        Like.objects.create(user = user, postingan = post)

        REDIRECT_EXP = [('/curhat/1/view/', 302)]
        resp = self.client.get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 0)

    def test_postingan_not_existed_komentar(self):
        REDIRECT_EXP = [('/curhat/', 302)]
        resp = self.client.get('/curhat/1000000/view/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)

    def test_postingan_exist_komentar_post_valid(self):
        resp = self.client.post('/curhat/1/view/', {'content': 'Ini komentar'})
        komentars = Komentar.objects.all()
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(komentars.count(), 1)
    
    def test_postingan_exist_komentar_post_invalid(self):
        resp = self.client.post('/curhat/1/view/', {'content': 'A' * 250})
        komentars = Komentar.objects.all()
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(komentars.count(), 0)

    def test_komentar_template(self):
        resp = self.client.get('/curhat/1/view/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'komentar/komentar.html')

class KomentarNonLoginTest(TestCase):
    def test_not_login_komentar(self):
        REDIRECT_EXP = [('/login/', 302)]
        resp = Client().get('/curhat/1/view/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)

    def test_not_login_like(self):
        REDIRECT_EXP = [('/login/', 302)]
        resp = Client().get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)

class KomentarApiLoginTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        self.user = User.objects.get(pk = 1)
        self.postingan = Postingan.objects.create(user = self.user, message = 'Halooo')
        self.client.login(username = 'test', password = 'HaloHalo12345678')
    
    def test_get_reaction_no_postid(self):
        EXP_JSON = {"error": "missing postinganId"} 
        resp = self.client.get("/curhat/getreaction/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_reaction_invalid_param(self):
        EXP_JSON = {"error": "cannot find postingan with id = postinganId"} 
        resp = self.client.get("/curhat/getreaction/?postinganId=100000")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_reaction_invalid_nonnumeric_param(self):
        EXP_JSON = {"error": "something went wrong"} 
        resp = self.client.get("/curhat/getreaction/?postinganId=asdsad")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_reaction_valid_param_no_reaction(self):
        EXP_JSON = {'reaction': 0}
        resp = self.client.get("/curhat/getreaction/?postinganId=1")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_reaction_valid_param_with_reaction(self):
        EXP_JSON = {'reaction': 2}
        Like.objects.create(postingan = self.postingan, user = self.user, reaction = 2)
        resp = self.client.get("/curhat/getreaction/?postinganId=1")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_post_komentar_invalid_method(self):
        EXP_JSON = {"error": "method not allowed"}

        resp = self.client.get('/curhat/sendkomentar/')
        
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 405)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_komentar_invalid_key(self):
        EXP_JSON = {"error": "invalid form"}

        data = {"Hahahu": "100000", "gagaga": "Ini postingannya mana?"}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)
        
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_post_komentar_invalid_form(self):
        EXP_JSON = {"error": "invalid form"}

        data = {"postinganId": "1", "content": "A" * 300}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)
        
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_komentar_invalid_nonnumeric_form(self):
        EXP_JSON = {"error": "something went wrong"} 
        
        data = {"postinganId": "asdsadas", "content": "A"}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)
        
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_komentar_no_postingan(self):
        EXP_JSON = {"error": "cannot find postingan with id = postinganId"}    
        
        data = {"postinganId": "100000", "content": "Ini postingannya mana?"}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)

        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_komentar_valid(self):
        EXP_JSON = {"status": "ok"}    
        
        data = {"postinganId": "1", "content": "Ini komen"}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)

        byk_komen = Komentar.objects.count()
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)
        self.assertEqual(byk_komen, 1)
    
    def test_post_reaction_invalid_method(self):
        EXP_JSON = {"error": "method not allowed"}
        resp = self.client.get('/curhat/sendreaction/')
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 405)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_reaction_invalid_postid(self):
        EXP_JSON = {'error': 'cannot find postingan with id = postinganId'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '100000', 'reaction': '2'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_reaction_invalid_key(self):
        EXP_JSON = {'error': 'invalid form'}
        resp = self.client.post("/curhat/sendreaction/", {'asd': '100000', 'fghgfh': '2'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_post_reaction_invalid_reaction(self):
        EXP_JSON = {'error': 'invalid form'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '1', 'reaction': '1000'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_reaction_invalid_nonnumeric_value(self):
        EXP_JSON = {'error': 'invalid form'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': 'asdsda', 'reaction': 'asdsad'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '1', 'reaction': 'asdsad'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_reaction_valid(self):
        EXP_JSON = {'status': 'ok'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '1', 'reaction': '2'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_reaction_update(self):
        Like.objects.create(postingan = self.postingan, user=self.user, reaction=1)
        EXP_JSON = {'status': 'ok'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '1', 'reaction': '2'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

        reaction = Like.objects.filter(postingan=self.postingan).get(user=self.user)
        self.assertEqual(reaction.reaction, 2)

class KomentarApiNonLoginTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        self.user = User.objects.get(pk = 1)
        self.postingan = Postingan.objects.create(user = self.user, message = 'Halooo')

        self.komentar = [] 
        for i in range(2):
            tmp = Komentar.objects.create(postingan = self.postingan, user = self.user, content = f"Ini Komentar {i}")
            self.komentar.append(tmp)
    
    def test_get_komentar_no_postid(self):
        EXP_JSON = {"error": "missing postinganId"} 
        resp = self.client.get("/curhat/getkomentar/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_komentar_invalid_param(self): 
        EXP_JSON = {"error": "cannot find postingan with id = postinganId"} 
        resp = self.client.get("/curhat/getkomentar/?postinganId=100000")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_komentar_invalid_nonnumeric_param(self): 
        EXP_JSON = {"error": "something went wrong"} 
        resp = self.client.get("/curhat/getkomentar/?postinganId=1&&start=asdasd")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)

        resp = self.client.get("/curhat/getkomentar/?postinganId=asdsad")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)

        resp = self.client.get("/curhat/getkomentar/?postinganId=11&&end=asdasd")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_komentar_valid_param_no_comment(self): 
        newPostingan = Postingan.objects.create(user = self.user, message = "post 2")

        EXP_JSON = {"count": 0, "content": []}
        resp = self.client.get(f"/curhat/getkomentar/?postinganId={newPostingan.pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_komentar_valid_param(self): 
        EXP_JSON = {
            "count": 2,
            "content": []
        }

        for elm in self.komentar:
            tmp = {"pk": elm.pk, "user": elm.user.username, "content": elm.content}
            EXP_JSON['content'].append(tmp)

        resp = self.client.get(f"/curhat/getkomentar/?postinganId={self.postingan.pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_get_komentar_valid_param_with_start_param(self): 
        EXP_JSON = {
            "count": 1,
            "content": []
        }

        tmp = {"pk": self.komentar[1].pk, "user": self.komentar[1].user.username, "content": self.komentar[1].content}
        EXP_JSON['content'].append(tmp)

        resp = self.client.get(f"/curhat/getkomentar/?postinganId={self.postingan.pk}&&start={self.komentar[1].pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_get_komentar_valid_param_with_end_param(self): 
        EXP_JSON = {
            "count": 1,
            "content": []
        }

        tmp = {"pk": self.komentar[0].pk, "user": self.komentar[0].user.username, "content": self.komentar[0].content}
        EXP_JSON['content'].append(tmp)

        resp = self.client.get(f"/curhat/getkomentar/?postinganId={self.postingan.pk}&&end={self.komentar[0].pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_last_komentar_no_postid(self):
        EXP_JSON = {"error": "missing postinganId"} 
        resp = self.client.get("/curhat/lastkomentar/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_last_komentar_invalid_param(self): 
        EXP_JSON = {"error": "cannot find postingan with id = postinganId"}
        resp = self.client.get("/curhat/lastkomentar/?postinganId=10000")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 400)
        self.assertEquals(resp_json, EXP_JSON)

    def test_last_komentar_invalid_nonumeric_param(self):
        EXP_JSON = {"error": "something went wrong"} 
        resp = self.client.get("/curhat/lastkomentar/?postinganId=asdsad")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 500)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_last_komentar_valid_param_no_comment(self): 
        newPostingan = Postingan.objects.create(user = self.user, message = "post 2")

        EXP_JSON = {"komentarLast": -1}
        resp = self.client.get(f"/curhat/lastkomentar/?postinganId={newPostingan.pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_last_komentar_valid_param(self): 
        EXP_JSON = {"komentarLast": self.komentar[1].pk}
        resp = self.client.get(f"/curhat/lastkomentar/?postinganId={self.postingan.pk}")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_post_komentar_nologin(self):
        EXP_JSON = {"error": "forbidden"}    
        
        data = {"postinganId": "1", "content": "Ini komen"}        
        resp = self.client.post('/curhat/sendkomentar/', data = data)

        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 403)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_get_reaction_nologin(self):
        EXP_JSON = {'error': 'forbidden'}
        resp = self.client.get("/curhat/getreaction/?postinganId=1")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 403)
        self.assertEquals(resp_json, EXP_JSON)
    
    def test_post_reaction_nologin(self):
        EXP_JSON = {'error': 'forbidden'}
        resp = self.client.post("/curhat/sendreaction/", {'postinganId': '1', 'reaction': '2'})
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 403)
        self.assertEquals(resp_json, EXP_JSON)