from django.contrib import admin
from komentar.models import Komentar, Like

class KomentarAdmin(admin.ModelAdmin):
    list_display = ('user', 'content', 'postingan')
    list_filter = ('user', 'content', 'postingan')
    search_fields = ('user', 'content', 'postingan')

    class Meta:
        model = Komentar

class LikeAdmin(admin.ModelAdmin):
    list_display = ('user', 'postingan')
    list_filter = ('user', 'postingan')
    search_fields = ('user', 'postingan')

    class Meta:
        model = Like


admin.site.register(Komentar, KomentarAdmin)
admin.site.register(Like, LikeAdmin)
