from django.urls import include, path
from komentar.views import index, post_like
from komentar.views import get_reaction, send_reaction
from komentar.views import get_komentar, last_komentar, send_komentar

urlpatterns = [
    path('<int:postid>/view/', index, name = 'index'),
    path('<int:postid>/like/', post_like, name = 'like'),
    path('<int:postid>/like/', post_like, name = 'like'),
    path('getkomentar/', get_komentar, name = 'getkomentar'),
    path('lastkomentar/', last_komentar, name = 'lastkomentar'),
    path('sendkomentar/', send_komentar, name = 'sendkomentar'),
    path('getreaction/', get_reaction, name = 'getreaction'),
    path('sendreaction/', send_reaction, name = 'sendreaction'),
]
