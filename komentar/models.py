from django.contrib.auth.models import User
from django.db import models
from postingan.models import Post as Postingan

class Komentar(models.Model):
    postingan = models.ForeignKey(Postingan, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    content = models.CharField(max_length = 200)

REACTION_CHOICE = [(0, 'expressionless'), (1, 'hearts'), (2, 'confused'), (3, 'angry')]
class Like(models.Model):
    postingan = models.ForeignKey(Postingan, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    reaction = models.IntegerField(choices = REACTION_CHOICE, default = 0)