from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

from django.db.models.deletion import CASCADE

# Create your models here.
class Work(models.Model):
    title = models.CharField(max_length=100)
    desc = models.TextField()
    duration = models.CharField(max_length=20)
    published = models.DateTimeField(editable=False, default=datetime.now, blank=True)
    user = models.ForeignKey(User, on_delete=CASCADE, default=None)
