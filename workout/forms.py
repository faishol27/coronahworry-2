from django import forms
from django.utils.translation import gettext_lazy as _ 
from .models import Work

class WorkForm(forms.ModelForm):
    class Meta:
        model = Work
        fields = ('title', 'desc', 'duration', 'user', )
        widgets = {'user' : forms.HiddenInput()}
    