from django.http import response
from django.test import TestCase, Client, client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from .models import Work
from akun.models import Profile
import json
       
# Create your tests here.

class TestWork(TestCase):
    client = Client()
  
    def setUp(self):
        user = User.objects.create_user(username="mumu", first_name="aku", last_name="cape", password="tes12345")
        Profile.objects.create(user=user)
        self.client.login(username="mumu", password="tes12345")
        self.activity_url = reverse("workout:listwork")

    def test_bmi(self):
       DEST = []
       response = self.client.get('/workout/', follow = True)
       self.assertEqual(response.redirect_chain, DEST)

    def test_create_url_exist(self):
        response = self.client.get("/workout/create/")
        self.assertEqual(response.status_code, 200)

    def test_list(self):
        response = self.client.get(self.activity_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "workout/list.html")

    def test_inValid_post(self):
        response = self.client.post('/workout/create/', {
            'title':' ', 
            'desc':'hadehhadehhadeh', 
            'duration':'0-30 menit',}) 
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'workout/create.html')


class TestAPIWork(TestCase):
    cient = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        self.user = User.objects.get(pk = 1)
        self.client.login(username = 'test', password = 'HaloHalo12345678')
        self.title = "haha"
        self.desc = "hehe"
        self.duration = "1.5-2 jam"

    def test_get_work_invalid(self):
        resp = self.client.get("/workout/cobalist/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        
    def test_last_komentar_no_postid(self):
        EXP_JSON = {
            "count":1,            
            "content": {},
          } 
        tmp = {"title": "haha", "user": self.user, "desc": "gdgdgd", "duration":"1-1.5 jam"}
        EXP_JSON['content'] = tmp
        resp = self.client.get("/workout/cobalist/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
    
    def test_post_work_login(self):
        EXP_JSON = {'content': []}  
        data = {"user":"test", "title": "jaaj", "desc": "hs", "duration":"1-1.5 jam"}        
        resp = self.client.post('/workout/cobalist/', data = data)

        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 200)
        self.assertEquals(resp_json, EXP_JSON)

    def test_cobaCreate_not_allowed(self):
        responJson = {'error': 'method not allowed'}
        resp = self.client.get("/workout/cobacreate/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 405)
        self.assertEquals(resp_json, responJson)

class TestAPIWorkNonLogin(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        self.user = User.objects.get(pk = 1)
    
    def test_get_komentar_invalid_nonnumeric_param(self): 
        EXP_JSON = {"error": "method not allowed"} 
        resp = self.client.get("/workout/cobacreate/")
        resp_json = json.loads(resp.content.decode())
        self.assertEqual(resp.status_code, 405)
        self.assertEquals(resp_json, EXP_JSON)

    
