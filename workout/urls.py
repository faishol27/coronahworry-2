from django.urls import path

from . import views

app_name = 'workout'

urlpatterns = [
    path('cobalist/', views.cobaList, name = 'list'),
    path('cobacreate/', views.cobaCreate, name = 'create'),
    path('list/', views.list_work, name='listwork'),
    path('create/', views.create_work, name='creatework'),
    path('', views.hitung_bmi, name='hitungbmi'),
]