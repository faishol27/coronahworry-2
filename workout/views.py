from django.shortcuts import render, redirect
from akun.models import Profile
from .models import Work
from .forms import WorkForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
 
# Create your views here.

# Fungsi untuk menampilkan hasil BMI dari user tertentu
def hitung_bmi(request):
    if request.user.is_authenticated:
        get_data = Profile.objects.get(user = request.user)
        bmi = get_data.hitung_bmi()
        print(bmi)
        context = {
                "get_data" : get_data,
                "bmi" : bmi
            }
        return render(request, "workout/hasilBMI.html", context)
    else:
        return redirect('main:login')

#Fungsi untuk menampilkan form dan membuat aktivitas baru
def create_work(request):
    if request.user.is_authenticated:
        id = request.user
        form = WorkForm(initial = {'user':id})
        if request.method == "POST":
            form = WorkForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect("workout:listwork")
        context = {'form':form}
        return render(request, 'workout/create.html', context)
    else:
        return redirect('main:login')

# fungsi Json
@csrf_exempt
def cobaCreate(request):
    responJson = {'error':'method not allowed'}
    status_code = 405

    if request.method == 'POST':
        title = request.POST.get('title')
        desc = request.POST.get('desc')
        duration = request.POST.get('duration')

        Work.objects.create(title=title, desc=desc, duration=duration)

        responJson = {'status': 'ok'}
        status_code = 200

        responJson["error"] = "something went wrong"
        status_code = 500

    return JsonResponse(responJson, status=status_code)
        
#Fungsi untuk menampilkan semua aktivitas yang telah dipost
def list_work(request):
    if request.user.is_authenticated:
        get_data = Profile.objects.get(user = request.user)
        bmi = get_data.hitung_bmi()
        print(bmi)
        works = Work.objects.all().order_by('-published')
        context = {'works':works, 'bmi':bmi}
        return render(request, 'workout/list.html', context)
    else:
        return redirect('main:login')

# Fungsi Json
def cobaList(request):
    if not request.user.is_authenticated:
        responJson = {'error': 'forbidden'}
        status_code = 403

    else :
        works = Work.objects.all()
        responContent = []
        for work in works :
            temp = {'user':work.user.username,'title':work.title, 'desc':work.desc, 'duration':work.duration, 'published':work.published}
            responContent.append(temp)
        responJson = {'content':responContent}
        status_code = 200
    return JsonResponse(responJson, status=status_code)