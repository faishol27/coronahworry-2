from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.shortcuts import redirect, render
from akun.models import Profile

def index(request):
    return render(request, 'main/index.html')

def akun_logout(request):
    logout(request)
    return redirect('main:login')

def akun_login(req):
    if not req.user.is_authenticated:
        authForm = AuthenticationForm()
        if req.method == 'POST':
            authForm = AuthenticationForm(data=req.POST.copy())
            if authForm.is_valid():
                user = authForm.get_user()
                if user != None:
                    login(req, authForm.get_user())
                    return redirect('/')
        context = { 'active': 'login', 'form': authForm }
        return render(req, 'main/login.html', context = context)
    else:
        return redirect('/')

def akun_register(req):
    if not req.user.is_authenticated:
        registerForm = UserCreationForm()
        if req.method == 'POST':
            registerForm = UserCreationForm(req.POST)
            if registerForm.is_valid():
                user = registerForm.save()
                Profile.objects.create(user = user)
                return redirect('main:login')
        context = { 'active': 'register', 'form': registerForm}
        return render(req, 'main/register.html', context = context)    
    else:
        return redirect('/')