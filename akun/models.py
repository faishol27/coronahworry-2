from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    nama = models.CharField(max_length=50,  null=True, blank=True)
    umur  = models.IntegerField(null=True, blank=True)
    domisili = models.CharField(max_length=50, null=True, blank=True)
    berat_badan = models.IntegerField(null=True, blank=True, default=0)
    tinggi_badan = models.IntegerField(null=True, blank=True, default=0)
    user = models.OneToOneField(User, on_delete = models.CASCADE)

    def hitung_bmi(self):
        if (self.berat_badan == 0 or self.tinggi_badan == 0):
            return 0;
        else:
            res = (self.berat_badan / ((self.tinggi_badan/100)**2))
            return round(res,2)
