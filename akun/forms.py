from django import forms
from django.contrib.auth.models import User
from .models import Profile

class UserForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['nama', 'umur', 'domisili', 'berat_badan', 'tinggi_badan']

    def edit_obj(self,profile_obj):
        profile_obj.nama = self.cleaned_data['nama']
        profile_obj.umur = self.cleaned_data['umur']
        profile_obj.domisili = self.cleaned_data['domisili']
        profile_obj.berat_badan = self.cleaned_data['berat_badan']
        profile_obj.tinggi_badan = self.cleaned_data['tinggi_badan']
        profile_obj.save()
