from django.shortcuts import render, redirect
from .models import Profile
from .forms import UserForm
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def profile(request): 
    return redirect('akun:profile_dua')
    

def profile_dua(request): # ambil data user
    if request.user.is_authenticated:
        get_data_dua = None
        try:
            get_data_dua = Profile.objects.get(user = request.user)
        except:
            get_data_dua = Profile.objects.create(user = request.user)

        context = {'data':get_data_dua}
        return render(request, 'akun/hasilProfile.html', context)
    else:
        return redirect('main:login')

def update_profile(request, pk): #update data user
    if request.user.is_authenticated:
        get_data_dua = Profile.objects.get(id=pk)
        get_profile = UserForm(instance = get_data_dua)
        
        if request.method == "POST":
            get_profile = UserForm(request.POST, instance=get_data_dua)
            if get_profile.is_valid():
                get_profile.edit_obj(get_data_dua)
                return redirect('akun:profile_dua')
        context = {'edit':get_profile, 'user':get_data_dua}
        return render(request, 'akun/editProfile.html', context)
    else:
        return redirect('main:login')

# fungsi Json
@csrf_exempt
def cobaCreate(request):
    responJson = {'error':'not allowed'}
    
    status_code = 405

    if request.method == 'POST':
        nama = request.POST.get('nama')
        print(nama)
        umur = request.POST.get('umur')
        domisili = request.POST.get('domisili')
        berat = request.POST.get('berat_badan')
        tinggi = request.POST.get('tinggi_badan')
        
        # desc = request.POST.get('desc')
        # duration = request.POST.get('duration')
        pk = request.POST.get('id')
        get_data_dua = Profile.objects.get(id=pk)
        get_data_dua.nama = nama
        get_data_dua.umur = umur
        get_data_dua.domisili = domisili
        get_data_dua.berat_badan = berat
        get_data_dua.tinggi_badan = tinggi

        get_data_dua.save()

        responJson = {'status': 'ok'}
        status_code = 200

    return JsonResponse(responJson, status=status_code)

def cobaList(request):
    if not request.user.is_authenticated:
        responJson = {'error': 'forbidden'}
        status_code = 403

    else :
        profile = Profile.objects.filter(user=request.user)
        responContent = []
        for i in profile :
            temp = {'nama':i.nama, 'umur':i.umur,'domisili':i.domisili, 'berat':i.berat_badan, 'tinggi':i.tinggi_badan}
            responContent.append(temp)
        responJson = {'content':responContent}
        status_code = 200
    return JsonResponse(responJson, status=status_code)

