from django.urls import path
from . import views

app_name ='akun'

urlpatterns = [
    path('', views.profile, name ='profile'),
    path('hasilprofile/', views.profile_dua , name = 'profile_dua'),
    path('editprofile/<int:pk>/', views.update_profile , name = 'update_profile'),
    path('cobalist/', views.cobaList, name = 'list'),
    path('cobacreate/', views.cobaCreate, name = 'create'),

]
