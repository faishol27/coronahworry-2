from django.test import TestCase, Client
from django.contrib.auth.models import User 
from django.urls import reverse, resolve
from akun.models import Profile
from akun.views import profile, profile_dua , update_profile

# Create your tests here.
class TestAkun(TestCase):
    
    def setUp(self):
        user = User.objects.create_user(username='test', first_name='akun', last_name='Testing', password='HaloHalo12345678')
        Profile.objects.create(user=user, nama='Test', umur=32,domisili='Jakarta',berat_badan=60,tinggi_badan=167)
        self.client = Client()
        self.client.login(username = 'test', password = 'HaloHalo12345678')
    
    #Check URL
    def test_profile_url_is_exist(self):
        user = User.objects.get(pk=1)
        response = self.client.get("/profile/hasilprofile/", follow=True)
        self.assertEqual(response.status_code, 200)
    
    
    def test_profile_edit_url_is_exist(self):
        response = self.client.get("/profile/editprofile/1")
        self.assertEquals(response.status_code, 301)

    #Check Views
    def test_profile_using_func_profile(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_hasil_profile_using_func_profile_dua(self):
        found = resolve('/profile/hasilprofile/')
        self.assertEqual(found.func, profile_dua)

    def test_edit_profile_using_func_update_profile(self):
        found = resolve('/profile/editprofile/1/')
        self.assertEqual(found.func, update_profile)

    #Check Templates
    def test_profile_using_hasil_profile_template(self):
        response = self.client.get('/profile/hasilprofile/', follow=True)
        self.assertTemplateUsed(response, 'akun/hasilProfile.html')

    def test_edit_profile_using_edit_profile_template(self):
        response = self.client.get('/profile/editprofile/1/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'akun/editProfile.html')

    def test_json_edit(self):
        dataJSON = {
            'id': '1',
            'nama':'nama',
            'umur':'23',
            'domisili':'domisili',
            'berat_badan':'23',
            'tinggi_badan':'100'
        }
        response = self.client.post('/profile/cobacreate/', dataJSON)
        self.assertEqual(response.status_code, 200)
        
    def test_json_view(self):
        resp = self.client.get('/profile/cobalist/')
        self.assertEqual(resp.status_code, 200)

    #Check models
    def test_can_save_profile_models(self):
        new_profile = Profile.objects.create(
            user = User.objects.create(username = 'josias'),
            nama = 'Josias',
            umur = 19,
            domisili = 'Depok',
            berat_badan = 70,
            tinggi_badan = 170,
        )

        check_all_profile = Profile.objects.all().count()
        self.assertEqual(check_all_profile, 2)

    
    
     
        
