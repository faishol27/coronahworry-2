from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Assessment(models.Model):
    yes = True
    no = False
    BOOLEAN_CHOICES = [(yes, 'Ya'), (no, 'Tidak/Tidak Tahu')]
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    suhu = models.BooleanField(choices=BOOLEAN_CHOICES)
    sesak = models.BooleanField(choices=BOOLEAN_CHOICES)
    batuk = models.BooleanField(choices=BOOLEAN_CHOICES)
    kontak_positif = models.BooleanField(choices=BOOLEAN_CHOICES)
    kontak_gejala = models.BooleanField(choices=BOOLEAN_CHOICES)
    status = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
