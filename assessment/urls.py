from django.urls import path

from . import views

app_name = 'assessment'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('result/<str:id>/', views.result, name='result'),
    path('result/all', views.results, name='results'),
    path('hide/<str:id>/', views.hide, name='hide'),
    path('test/', views.index, name='index')

]
