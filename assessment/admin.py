from django.contrib import admin
from assessment.models import Assessment


class AssessmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'suhu', 'sesak', 'batuk',
                    'kontak_positif', 'kontak_gejala', 'created_at', 'status')
    list_filter = ('user', 'suhu', 'sesak', 'batuk',
                   'kontak_positif', 'kontak_gejala', 'created_at', 'status')
    search_fields = ('user', 'suhu', 'sesak', 'batuk',
                     'kontak_positif', 'kontak_gejala', 'created_at', 'status')

    class Meta:
        model = Assessment


admin.site.register(Assessment, AssessmentAdmin)
