from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from assessment.views import index, landing, result, results, hide
from assessment.models import Assessment
from django.apps import apps
from assessment.apps import AssessmentConfig
# Create your tests here.


class AssessmentTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(
            username='test', first_name='Akun', last_name='Testing', password='HaloHalo12345678')
        user = User.objects.get(pk=1)
        self.client.login(username='test', password='HaloHalo12345678')

    def test_apps(self):
        self.assertEqual(AssessmentConfig.name, 'assessment')
        self.assertEqual(apps.get_app_config('assessment').name, 'assessment')

    def test_assessment_url_is_exist(self):
        response = self.client.get('/assessment/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_assessment_result_id_url_is_exist(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True,)
        response = self.client.post('/assessment/result/1', follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_assessment_landing_url_is_exist(self):
        response = self.client.get('/assessment/')
        self.assertEqual(response.status_code, 200)
    
    def test_assessment_results_url_is_exist(self):
        response = self.client.get('/assessment/result/all')
        self.assertEqual(response.status_code, 200)
        
    def test_assessment_landing_using_right_template(self):
        response = self.client.get('/assessment/')
        self.assertTemplateUsed(
            response, 'assessment/landing.html')
        
    def test_assessment_index_using_right_template(self):
        response = self.client.get('/assessment/test/')
        self.assertTemplateUsed(
            response, 'assessment/assessment.html')
    
    def test_assessment_results_using_right_template(self):
        response = self.client.get('/assessment/result/all')
        self.assertTemplateUsed(
            response, 'assessment/results.html')
        
    def test_assessment_result_id_using_right_template(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True)
        response = self.client.get('/assessment/result/1/')
        self.assertTemplateUsed(
            response, 'assessment/high.html')
        
    def test_assessment_result_id_medium_using_right_template(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=False,
                                  kontak_gejala=False)
        response = self.client.get('/assessment/result/1/')
        self.assertTemplateUsed(
            response, 'assessment/medium.html')
        
    def test_assessment_result_id_low_using_right_template(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=False,
                                  sesak=False,
                                  batuk=False,
                                  kontak_positif=False,
                                  kontak_gejala=False)
        response = self.client.get('/assessment/result/1/')
        self.assertTemplateUsed(
            response, 'assessment/low.html')
        
    def test_create_assessment(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True)
        counting_all_available_assessment = Assessment.objects.all().count()
        self.assertEqual(counting_all_available_assessment, 1)

    def test_hide_assessment(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True,status=1)
        asses = Assessment.objects.get(user=user, status =1)
        self.assertEqual(asses.status, 1)
