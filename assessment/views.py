from django.shortcuts import render, redirect
from assessment.models import Assessment
from assessment.forms import AssessmentForm
from django.http import JsonResponse

# Create your views here.


def index(request):
    if request.user.is_authenticated:
        form = AssessmentForm(request.POST or None)

        if request.POST.get('action') == 'post':
            data = request.POST
            form = Assessment(user=request.user, suhu=bool(data['suhu']), batuk=bool(data['batuk']), sesak=bool(data['sesak']),
                              kontak_positif=bool(data['kontak_positif']), kontak_gejala=bool(data['kontak_gejala']))
            form.save()
            response = {}
            response['suhu'] = bool(data['suhu'])
            response['batuk'] = bool(data['batuk'])
            response['sesak'] = bool(data['sesak'])
            response['kontak_positif'] = bool(data['kontak_positif'])
            response['kontak_gejala'] = bool(data['kontak_gejala'])
            response['id'] = form.id

            return JsonResponse(response)

        context = {
            'form': form
        }
        return render(request, 'assessment/assessment.html', context)
    else:
        return redirect('main:login')


def landing(request):
    return render(request, 'assessment/landing.html')


def result(request, id):
    data = Assessment.objects.filter(user=request.user, id=id, status=1)
    if(len(data) != 0):
        counterGejala = 0
        if(data[0].suhu == True):
            counterGejala += 1
        if(data[0].sesak == True):
            counterGejala += 1
        if(data[0].batuk == True):
            counterGejala += 1

        if(counterGejala == 0):
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/low.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/low.html')
            else:
                return render(request, 'assessment/low.html')

        elif(counterGejala == 1):
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            else:
                return render(request, 'assessment/low.html')

        else:
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/high.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/high.html')
            else:
                return render(request, 'assessment/medium.html')
    else:
        return render('assessment:index')


def results(request):
    if request.user.is_authenticated:
        datas = Assessment.objects.all().filter(user=request.user, status=1)
        context = {
            'datas': datas
        }
        return render(request, 'assessment/results.html', context)
    else:
        return redirect('main:login')


def hide(request, id):
    selected_data = Assessment.objects.get(
        user=request.user, id=id)
    selected_data.status = 0
    selected_data.save()
    return redirect('assessment:results')
