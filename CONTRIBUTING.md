# Prosedur Mengerjakan Tugas
1. Lakukan clone pada repository ini dengan `git clone https://gitlab.com/faishol27/coronahworry.git`.
2. Lalu, install python module yang dibutuhkan dengan `pip install -r requirements.txt`.
3. Buat branch baru dari repository degan perintah `git checkout -b <nama-branch>`.
4. Kemudian lakukan migrasi database dengan cara `python manage.py makemigrations`, lalu `python manage.py migrate`.
5. Kerjakan app kalian.
6. Uncomment url app kalian yang berada di `coronahworry/urls.py`.
7. Tambahkan menu app kalian pada templates dengan menambahkan kode di bawah ini ke `templates/base/navbar.html` di bagian dalam tag `if`:
    ```
    <li id="nama-app-navbar" class="nav-item">
        <a class="nav-link" href="{% url 'nama-app:index' %}">Nama App</a>
    </li>
    ```
8. Jangan lupa untuk membuat file tests.py pada app kalian.
9. Apabila ingin melakukan push, lakukan perintah berikut `git push origin <nama-branch>`

# Butuh Field User
Apabila pada model kalian memerlukan field user, silakan gunakan model User dari django.contrib.auth.models
