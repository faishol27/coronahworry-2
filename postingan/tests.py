from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
from . import models
from .models import Post
from .views import post_list
import json

# Create your tests here.

#Check account
class PostTest(TestCase):
    def setUp(self):
        User.objects.create_user(username='konpeko', password='holofantasy')
        self.client = Client()
        self.client.login(username='konpeko', password='holofantasy')

#Check models
    def test_create_post(self):
        user = User.objects.get(pk=1)
        Post.objects.create(user=user, message='test test test')
        self.assertEqual(Post.objects.all().count(), 1)

#Check URL
    def test_index_url_exist(self):
        response = self.client.get("/curhat/")
        self.assertEqual(response.status_code, 200)

#Check Views
    def test_post_list_exist(self):
        response = resolve("/curhat/")
        self.assertEqual(response.func, post_list)

    def test_post_create(self):
        res = {'result':'no'}
        response = self.client.get("/curhat/create/")
        jsonresponse = json.loads(response.content.decode())
        self.assertEqual(response.status_code, 200)
        self.assertEqual(res, jsonresponse)

    def test_post_create_POST(self):
        res = "Success"
        response = self.client.post("/curhat/create/", data = {"content":"hai hai halo"})
        jsonresponse = json.loads(response.content.decode())
        posts = Post.objects.all()
        self.assertEqual(response.status_code,200)
        self.assertEqual(posts.count(), 1)
        self.assertEqual(res, jsonresponse["result"])

    def test_post_get(self):
        response = self.client.get("/curhat/list/")
        self.assertEqual(response.status_code, 200)

#Check Templates
    def test_index_using_template(self):
        response = self.client.get("/curhat/")
        self.assertTemplateUsed(response, "post/post.html")

class PostTest2(TestCase):
#Check page redirect
    def test_redirect_login(self):
        response = Client().get("/curhat/")
        self.assertEqual(response.status_code, 302)