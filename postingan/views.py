from django.shortcuts import render,redirect
from .models import Post
from .forms import Postform
from django.http import JsonResponse

# Create your views here.
def post_list(request):
    if request.user.is_authenticated:
        posts = Post.objects.all()
        id = request.user
        form = Postform(initial={'user': id})
        if request.method == 'POST':
            form = Postform(request.POST)
            if form.is_valid():
                form.save()
                return redirect('postingan:index')
        context = {'post':posts, 'form': form}
        return render(request, "post/post.html", context)

    else:
        return redirect('main:login')

def post_create(request):
    res = {'result':'no'}
    if request.method == 'POST':
        prev = Post.objects.all().count()
        content = request.POST.get('content')
        user = request.user

        obj = Post.objects.create(message=content, user=user)
        
        item = {'message':obj.message, 'time': obj.time.strftime("%m/%d/%Y, %H:%M"), 'user':obj.user.username, 'id':obj.id}
        res["item"] = item 
        crnt = Post.objects.all().count()
        if prev+1==crnt:
            res['result'] = 'Success'
        else:
            res['result'] = 'Failed'
    
    return JsonResponse(res)

def post_get(request):
    res = {}

    if request.user.is_authenticated:
        items = []
        for i in Post.objects.all():
            items.append({'message':i.message, 'time':i.time.strftime("%m/%d/%Y, %H:%M"),'user':i.user.username, 'id':i.id})
        res = {'content':items, 'result':'Success'}
    
    return JsonResponse(res)