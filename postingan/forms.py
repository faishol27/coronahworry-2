from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Post

class Postform(forms.ModelForm):    
    class Meta:
        model = Post
        fields = ("message", "user")
        widgets = {'user': forms.HiddenInput()}