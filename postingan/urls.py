from django.urls import path

from . import views

app_name = 'Postingan'

urlpatterns = [
    path('', views.post_list, name='index'),
    path('create/', views.post_create, name='create'),
    path('list/', views.post_get, name='list'),
]